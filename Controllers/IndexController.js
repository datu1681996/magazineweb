var express = require('express');
var router = express.Router();
const mod = require('../Common/Mod')
const tableCrud = require('../DataAccess/dynamoDB/TablesCRUD')
const itemCrud = require('../DataAccess/dynamoDB/ItemsCRUD')
const categories = require('../Models/Categories')
const posts = require('../Models/Posts')

/* GET home page. */
router.get('/', function(req, res, next) {
    // hard code DATA
    // REMOVE AFTER done create real table
    const postParam = posts.PostTableParam().paramCreate()
    tableCrud.createTables(postParam, res, onCreateDone = function(){
        // load hard DATA
        // const postParamUpdate = posts.PostTableParam('TestPost1').paramUpdate()
        // tableCrud.loadTables(postParamUpdate, res, './DataAccess/JsonData/posts.json')
        // get categories -> outsite ???
        
        // get posts
        
        tableCrud.scanTables('Categories', function (cateData) {
            tableCrud.scanTables('Posts', function (postData) {
                loadPostsToView(res, cateData, postData)
            })
        })
    })
   
    // res.end("end data")
});

// list item by tag (category)
router.get('/t/:cate', function(req, res, next) {
    // get posts by category
    console.log("cate = "+ req.params.cate)
    const searchParam = posts.PostTableParam()
                            .searchByCate(req.params.cate)
    tableCrud.scanTables('Categories', function (cateData) {
        tableCrud.scanTables_Param(searchParam, onSuccess = function(postData){
            loadPostsToView(res, cateData, postData)
        })
    })
    
});

router.get('/:author&:title', function(req, res, next) {
    console.log("params =", req.params)
    console.log("author =", req.params.author)
    console.log("title =", req.params.title)
    const getPostParam = posts.PostItemParam()
                        .paramGet().getItem(req.params.author, req.params.title)
    itemCrud.get_Items(getPostParam, res, onSuccess = function(postData){
       loadPostDetailToView(res, postData)
    })
       
});

router.post('/addPost', function (req, res) {
    // First read existing post.
    // console.log("ADD DONE")
    // res.send('respond with a resource');
    // res.redirect('/');
})

//// Helper functions
// function reduce call back by 2 here 
// load List of Post To View
function loadPostsToView(res, cateData, postData) {
    let htmlCate = ""
    let htmlCateMore = ""
    let htmlPost = ""

    let someData = {
        "some1": {
            "summary": "How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?",
            "blogger": "Katy Liuyyyyyyyyy",
            "createdTime": "Sep 29, 2017 at 9:48 am"
        },
        "some2": {
            "summary": "AAAAAA",
            "blogger": "Katy 2",
            "createdTime": "Sep 30, 2017 at 9:48 am"
        },
        "some3": {
            "summary": "BBBBBBCCC",
            "blogger": "Katy 2",
            "createdTime": "Sep 30, 2017 at 9:48 am"
        }
    }

    const cateList = cateData.Items
    // Head of sections
    htmlCate += '<ul>';     htmlCateMore += '<ul>'
    // 1st tag item
    htmlCate += '<li class="active"><a href="/">all</a></li>'
    
    let i = 0 // counter for both display & more tag
    for (item in cateList) {
        const cateItem = cateList[item]
        if (i < 4)  // tags for display
            htmlCate += '<li><a href="/t/' + cateItem.title + '">' + cateItem.title + '</a></li>'
        else    // more tags
            htmlCateMore += '<li><a href="/t/' + cateItem.title + '">' + cateItem.title + '</a></li>'
    }

    // Tail of sections
    htmlCate += '</ul>';    htmlCateMore += '</ul>'

    // post items
    for (item in postData.Items) {
        const postItem = postData.Items[item]
        console.log("item auth=", postItem.author)
        htmlPost+= '<div class="card card_small_with_image grid-item">'
        // check when no image
        if (postItem.content.image == undefined || postItem.content.image =="NoImage" )
            htmlPost+= '<img class="card-img-top" src="../images/post_10.jpg" alt="">'
        else
            htmlPost+= '<img class="card-img-top" src="' +postItem.content.image+ '" alt="">'

        htmlPost+= '<div class="card-body">'
        htmlPost+= '<div class="card-title card-title-small"><a href="' 
                                        + postItem.author 
                                        + "&"
                                        + postItem.title + '">'
                                        + postItem.title +'</a></div>'
        htmlPost+= '<small class="post_meta"><a href="#">'+ postItem.author +'</a><span>'+ postItem.content.createdTime +'</span></small>'
        htmlPost+= '</div>'
        htmlPost+= '</div>'
    }

    mod.fs.readFile(mod.viewHtml().catePage, onSuccess = function (err, data) {
        mod.callBackHTMLRes(err, data, res, function (index) {
            // Hide these tags until append data to them
            index('.sidebar').remove()
            index('#load_more').remove()
            console.log("handle index(Cate) PAGE")

            index('.section_tags').append(htmlCate)
            index('.section_MoreTags').append(htmlCateMore)
            index('.clearfix').append(htmlPost)
            
            // for (item in someData) {
            //     // to load different type of page
                // console.log("item =", Object.keys(someData).indexOf(item))
                // if(Object.keys(someData).indexOf(item)%2 !== 0) {
                
                // }
            // }

        }, endMessage = "end post data here")
    })
}

// load Post Detail To View
function loadPostDetailToView(res, postData) {
    mod.fs.readFile(mod.viewHtml().postPage, function (err, viewData) {
        mod.callBackHTMLRes(err, viewData, res, function (post) {
            // Hide these tags until append data to them
            post('.sidebar').remove()

            post('.similar_posts').remove()
            post('.post_comment').remove()
            post('.comments').remove()
            post('#load_more').remove()

            const postItem = postData.Item
            const postContent = postData.Item.content
            console.log("handle post Detail PAGE")
            if (postItem.content.image != undefined && postItem.content.image !="NoImage" )
                post('.parallax-window').attr('data-image-src', postContent.image);

            post('.authorUser').text(postItem.author)
            post('.postCreatedDate').text(postContent.createdTime)
            post('.post_content').text(postContent.description)

        }, "end post Detail data here")
    })
}

module.exports = router;
