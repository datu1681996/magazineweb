var express = require('express');
var router = express.Router();
const mod = require('../../Common/Mod')
// const bcrypt = require('bcryptjs');
const passport = require('passport');
const tableCrud = require('../../DataAccess/dynamoDB/TablesCRUD')
const itemCrud = require('../../DataAccess/dynamoDB/ItemsCRUD')
const users = require('../../Models/Users')

const _view = mod.viewHtml()

// // Passport Config
// require('../../config/passport')(passport);
// // Passport Middleware
// router.use(passport.initialize());
// router.use(passport.session());

// Login Form
router.get('/login', function(req, res){
    res.render(_view.AdLogin);
});

// Login Process
router.post('/login', function(req, res, next){
    // const loginAuth = {
    //     uName: req.body.userName,
    // pass: req.body.password
    // }
    const uName = req.body.username
    const pass = req.body.password
    console.log('user name & pass =', uName + ' - ' + pass)
    const userSearchParam = users.UserItemParam()
                          .paramGet()
                          .getItem('Admin', uName)

    // tableCrud.scanTables_Param(userSearchParam, onSuccess = function(data) {
    //     console.log('found = ', data.Item.info.userName)
    //     res.redirect('/admin');
    // })
    
    itemCrud.get_Items(userSearchParam, res, onSuccess = function(data) {
        if (!data.Item){
            mod.isLogin = false // happen when re-login wrong data
            res.redirect('/admin/login')
        } else {
            if(pass == data.Item.info.password) {
                console.log('found = ', data.Item.userId)
                console.log('with pass = ', data.Item.info.password)
                mod.isLogin = true
                res.redirect('/admin')
            } else {
                mod.isLogin = false
                res.redirect('/admin/login')
            }
        }
        
    })

    // passport.authenticate('local', {
    //     successRedirect:'/admin',
    //     failureRedirect:'/admin/login'
    //     // ,
    //     // failureFlash: true
    // })(req, res, next);
});

// logout
router.get('/logout', function(req, res){
    req.logout();
    // req.flash('success', 'You are logged out');
    mod.isLogin = false
    res.redirect('/admin/login');
});
  
/* GET admin post page. */
router.get('/', checkAdmin, function(req, res, next) {
    // if (!exports.isLogin){
    //     res.render(_view.AdLogin);
    // }
    // else
    res.render(_view.AdManager, {
        title:'this is admin manage page'
    });
    
    
});

// check if admin login
function checkAdmin(req, res, next){
    if(mod.isLogin){
      next();
    }else{
      res.redirect('/admin/login');
    }
}

module.exports = router;