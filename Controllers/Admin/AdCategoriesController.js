var express = require('express');
var router = express.Router();

const mod = require('../../Common/Mod')
const tableCrud = require('../../DataAccess/dynamoDB/TablesCRUD')
const itemCrud = require('../../DataAccess/dynamoDB/ItemsCRUD')
const categories = require('../../Models/Categories')

const _view = mod.viewHtml()
const cateItem = categories.CategoryItemParam()

/* GET admin categories page. */
router.get('/', checkAdmin, function(req, res, next) {
    // get categories
    tableCrud.scanTables('Categories', function (data) {
        res.render(_view.AdIndexCate, {
            title:'Categories Manager',
            categories: data.Items
        });
    })
});

// Get admin search type category
router.get('/sType/:typeCate', checkAdmin, function(req, res, next) {
    console.log("type Category = "+ req.params.typeCate)
    const searchParam = cateItem.paramGet()
                            .searchItemByCateType(req.params.typeCate)
    itemCrud.searchItems(searchParam, res, onSuccess = function(data){
        res.render(_view.AdIndexCate, {
            title:'Categories Manager',
            categories: data.Items
        });
    })
});

// Get admin detail category
router.get('/:cateType&:title', checkAdmin, function(req, res, next) {
    itemCrud.get_Items(getCateParam(req), res, onSuccess = function(cateData){
        res.render(_view.AdDetailCate, {
            title:'Category detail',
            category: cateData.Item
        });
    })
});

// Load Add Category Form
router.get('/add', checkAdmin, function(req, res){
    res.render(_view.AdAddCate, {
        title:'Add Category'
    });
});

// Search submit
router.post('/typeCate', function(req, res){
    const type = req.body.typeCate
    console.log('param typeCate =', type)
    switch (type.trim()) {
        case "": res.redirect('/admin/categories'); break
        default: res.redirect('/admin/categories/sType/' + type)
    }    
})

// Add Submit POST Route
router.post('/add', function(req, res){
    let cateInfo = {
        "summary": req.body.summary,
        "description": req.body.description,
        "createdTime": mod.getCurrentDate()
    }

    const createCategoryParam = cateItem.paramCreate(req.body.cateType
                                            , req.body.title
                                            , cateInfo)

    itemCrud.createItems(createCategoryParam, res, onSuccess = function(data){
        res.redirect('/admin/categories')
    })
});

// Load Edit Category Form
router.get('/edit/:cateType&:title', checkAdmin, function(req, res){
    itemCrud.get_Items(getCateParam(req), res, onSuccess = function(cateData){
        res.render(_view.AdEditCate, {
            title:'Edit Category',
            category: cateData.Item
        });
    })
});
  
// Update Submit POST Route
// use PUT ??? -> use Ajax ?
router.post('/edit/:cateType&:title', function(req, res){
    let cateInfo = {
        "summary": req.body.summary,
        "description": req.body.description,
        "createdTime": mod.getCurrentDate()
    }

    // update keys ???    
    const updateCateParam = cateItem.paramUpdate(req.params.cateType
                                            , req.params.title
                                            , cateInfo)

    itemCrud.updateItems(updateCateParam, res, onSuccess = function(data){
       console.log('update Category done!')
        // write file
        
        res.redirect('/admin/categories')
    })
});

// Load Delete Category Form
router.get('/delete/:cateType&:title', checkAdmin, function(req, res){
    itemCrud.get_Items(getCateParam(req), res, onSuccess = function(cateData){
        res.render(_view.AdDeleteCate, {
            title:'Do you want to delete this Category?',
            category: cateData.Item
        });
    })
});

// Delete Category
router.delete('/:cateType&:title', function(req, res){
    const deleteCateParam = cateItem.paramDelete(req.params.cateType
                                            , req.params.title)
    itemCrud.deleteItems(deleteCateParam, res, onSuccess = function(data){
        console.log('delete Category done!')
        res.send('Success')
        // write file

    })
});

// Helper functions
// Category - get param
getCateParam = function(req){
    return cateItem.paramGet()
                .getItem(req.params.cateType, req.params.title)
} 

// check if admin login
function checkAdmin(req, res, next){
    if(mod.isLogin){
      next();
    }else{
      res.redirect('/admin/login');
    }
}

module.exports = router;