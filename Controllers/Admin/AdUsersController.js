var express = require('express');
var router = express.Router();

const mod = require('../../Common/Mod')
const tableCrud = require('../../DataAccess/dynamoDB/TablesCRUD')
const itemCrud = require('../../DataAccess/dynamoDB/ItemsCRUD')
const users = require('../../Models/Users')
// const categories = require('../../Models/Categories')

const _view = mod.viewHtml()
const userItem = users.UserItemParam()
let userIdCount = '0'

/* GET admin users page. */
router.get('/', checkAdmin, function(req, res, next) {
    // get users
    tableCrud.scanTables('Users', function (data) {
        // get last user -> then get num for add next user base on its id
        userIdCount = data.Items[data.Count-1].userId.slice( 1 )

        res.render(_view.AdIndexUser, {
            title:'Users Manager',
            users: data.Items
        });
    })
});

// Get admin search user role
router.get('/sRoll/:role', checkAdmin, function(req, res, next) {
    console.log("role = "+ req.params.role)
    const searchParam = userItem.paramGet()
                            .searchByRole(req.params.role)
    itemCrud.searchItems(searchParam, res, onSuccess = function(data){
        res.render(_view.AdIndexUser, {
            title:'Users Manager',
            users: data.Items
        });
    })
});

// Get admin detail user
router.get('/:role&:userId', checkAdmin, function(req, res, next) {
    itemCrud.get_Items(getUserParam(req), res, onSuccess = function(data){
        res.render(_view.AdDetailUser, {
            title: 'User detail',
            user: data.Item
        });
    })
});

// Load Add user Form
router.get('/add', checkAdmin, function(req, res){
    res.render(_view.AdAddUser, {
        title:'Add User'
    });
});

// Search submit
router.post('/role', function(req, res){
    const type = req.body.role
    console.log('param role =', type)
    switch (type.trim()) {
        case "": res.redirect('/admin/users'); break
        default: res.redirect('/admin/users/sRoll/' + type)
    }    
})

// Add Submit POST Route
router.post('/add', function(req, res){
    let userInfo = {
        "fullName": req.body.fullName,
        "userName": req.body.userName,
        "password": req.body.password,
        "email": req.body.email
    }
    console.log('UID when post = ' + userIdCount)
    const newID = parseInt(userIdCount) + 1
    const uid = 'U' + newID

    const createUserParam = userItem.paramCreate(req.body.dropDownRole
                                            // , req.body.userId
                                            , uid
                                            , userInfo)

    itemCrud.createItems(createUserParam, res, onSuccess = function(data){
        res.redirect('/admin/users')
    })
});

// Load Edit User Form
router.get('/edit/:role&:userId', checkAdmin, function(req, res){
    itemCrud.get_Items(getUserParam(req), res, onSuccess = function(data){
        res.render(_view.AdEditUser, {
            title:'Edit User',
            user: data.Item
        });
    })
});
  
// Update Submit POST Route
// use PUT ??? -> use Ajax ?
router.post('/edit/:role&:userId', function(req, res){
    let userInfo = {
        "fullName": req.body.fullName,
        "userName": req.body.userName,
        "password": req.body.password,
        "email": req.body.email
    }

    // update keys ???    
    const updateUserParam = userItem.paramUpdate(req.params.role
                                            , req.params.userId
                                            , userInfo)

    itemCrud.updateItems(updateUserParam, res, onSuccess = function(data){
       console.log('update User done!')
        // write file
        
        res.redirect('/admin/users')
    })
});

// Load Delete User Form
router.get('/delete/:role&:userId', checkAdmin, function(req, res){
    itemCrud.get_Items(getUserParam(req), res, onSuccess = function(userData){
        res.render(_view.AdDeleteUser, {
            title:'Do you want to delete this User?',
            user: userData.Item
        });
    })
});

// Delete User
router.delete('/:role&:userId', function(req, res){
    const deleteUserParam = userItem.paramDelete(req.params.role
                                            , req.params.userId)
    itemCrud.deleteItems(deleteUserParam, res, onSuccess = function(data){
        console.log('delete User done!')
        res.send('Success')
        // write file

    })
});

// Helper functions
// User - get param
getUserParam = function(req){
    return userItem.paramGet()
                .getItem(req.params.role, req.params.userId)
} 

// check if admin login
function checkAdmin(req, res, next){
    if(mod.isLogin){
      next();
    }else{
      res.redirect('/admin/login');
    }
}

module.exports = router;