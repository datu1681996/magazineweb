var express = require('express');
var router = express.Router();

const mod = require('../../Common/Mod')
const tableCrud = require('../../DataAccess/dynamoDB/TablesCRUD')
const itemCrud = require('../../DataAccess/dynamoDB/ItemsCRUD')
const posts = require('../../Models/Posts')
const m = require('../Admin/ManagerController')

const _view = mod.viewHtml()

/* GET admin post page. */
router.get('/', checkAdmin, function(req, res, next) {
    console.log('islogin =', mod.isLogin)
    // hard code DATA
    // REMOVE AFTER done create real table
    const postParam = posts.PostTableParam().paramCreate()
    tableCrud.createTables(postParam, res, onCreateDone = function(){
        // load hard DATA
        // const postParamUpdate = posts.PostTableParam('TestPost1').paramUpdate() 
        // tableCrud.loadTables(postParamUpdate, res, './DataAccess/JsonData/posts.json')

        // get posts
        tableCrud.scanTables('Posts', function (data) { // still not sync ???
            res.render(_view.AdIndexPost, {
                title:'Posts Manager',
                posts: data
            });
        })
    })
});

// Get admin search author post
router.get('/s/:author', checkAdmin, function(req, res, next) {
    console.log("author = "+ req.params.author)
    const searchParam = posts.PostItemParam()
                            .paramGet()
                            .searchItemByAuthor(req.params.author)
    itemCrud.searchItems(searchParam, res, onSuccess = function(data){
        res.render(mod.viewHtml().AdIndexPost, {
            title:'Posts Manager',
            posts: data
        });
    })
});

// Get admin detail post
router.get('/:author&:title', checkAdmin, function(req, res, next) {
    itemCrud.get_Items(getPostParam(req), res, onSuccess = function(postData){
        console.log('admin load post DETAIL:', JSON.stringify(postData, null, 2))
        res.render(_view.AdDetailPost, {
            title:'Post detail',
            post: postData.Item
        });
    })
});

// Load Add Post Form
router.get('/add', checkAdmin, function(req, res){
    tableCrud.scanTables('Categories', function (data) {
        res.render(_view.AdAddPost, {
            title:'Add Post',
            categories: data['Items']
        });
    })
});

// Search submit
router.post('/author', function(req, res){
    const author = req.body.author
    console.log('param author =', author)
    switch (author.trim()) {
        case "": res.redirect('/admin/posts'); break
        default: res.redirect('/admin/posts/s/' + author)
    }    
})

// Add Submit POST Route
router.post('/add', function(req, res){
    console.log('param add category =', req.body.dropDownCate)

    //// demo S3
    const s3 = new mod.AWS.S3()
    const myBucket = 'magazinewebimg'
    // mod.fs.readFile('./PresentationLayer/images/category.jpg', function (err, data) {
    //     if (err) { throw err; }
    //     params = {Bucket: myBucket, Key: 'categoryimg', Body: data };
    
    //     s3.putObject(params, function(err, data) {
    //         if (err) {
    //             console.log(err)
    //         } else {
    //             console.log("Successfully uploaded data to myBucket/myKey");
    //         }
    //     });
    // });
    ////

    let postContent = {
        "summary": req.body.summary,
        "description": req.body.description,
        "createdTime": mod.getCurrentDate(),
        // "category": req.body.category
        "category": req.body.dropDownCate,
        "image": "https://s3-us-west-2.amazonaws.com/magazinewebimg/2015-fashion-trends.jpg"
    }

    const createPostParam = posts.PostItemParam()
                                .paramCreate(req.body.author
                                            , req.body.title
                                            , postContent)

    itemCrud.createItems(createPostParam, res, onSuccess = function(data){
        // write file

        // req.flash('Post Add');
        res.redirect('/admin/posts')
    })
});

// Load Edit Post Form
router.get('/edit/:author&:title', checkAdmin, function(req, res){
    tableCrud.scanTables('Categories', function (cateData) {
        itemCrud.get_Items(getPostParam(req), res, onSuccess = function(postData){
            res.render(_view.AdEditPost, {
                title:'Edit Post',
                post: postData.Item,
                categories: cateData.Items
            });
        })
    })
    
});
  

// getImg = function(req) {
//     itemCrud.get_Items(getPostParam(req), res, onSuccess = function(postData){
//         return postData.Item.content.image
//     })
// }
// Update Submit POST Route
// use PUT ??? -> use Ajax ?
router.post('/edit/:author&:title', function(req, res){
    
    let postContent = {
        "summary": req.body.summary,
        "description": req.body.description,
        "createdTime": mod.getCurrentDate(),
        "category": req.body.dropDownCate
        ,
        "image": "https://s3-us-west-2.amazonaws.com/magazinewebimg/2015-fashion-trends.jpg"
    }

    // update keys ???    
    const updatePostParam = posts.PostItemParam()
                                .paramUpdate(req.params.author
                                            , req.params.title
                                            , postContent)

    itemCrud.updateItems(updatePostParam, res, onSuccess = function(data){
       console.log('update Post done!')
        // write file

        mod.fs.readFile('./DataAccess/JsonData/posts.json', 'utf8', function (err, data) {
            data = JSON.parse( data )
            // console.log('print JSON Post: ', JSON.stringify(data))
            data.forEach(function (item) {
                if (item.author === req.params.author
                      && item.title === req.params.title) {
                    // item.content.description = req.body.description
                    // item.content.createdTime = '2018-11-18'
                    // item.content.category = req.body.category
                    // item.content.image = "No Image"
                    // console.log('update JSON Post item: ', JSON.stringify(item))
                }
            })

            // mod.fs.writeFile ('./DataAccess/JsonData/posts.json', JSON.stringify(data), function(err) {
            //     if (err) throw err;
            //     console.log('add to json complete');
            //     }
            // );
        });
        
        // req.flash('Post Updated');
        res.redirect('/admin/posts')
        // res.send('Success')
    })
});

// Load Delete Post Form
router.get('/delete/:author&:title', checkAdmin, function(req, res){
    itemCrud.get_Items(getPostParam(req), res, onSuccess = function(postData){
        res.render(_view.AdDeletePost, {
            title:'Do you want to delete this Post?',
            post: postData.Item
        });
    })
});

// Delete Article
router.delete('/:author&:title', function(req, res){
    const deletePostParam = posts.PostItemParam()
                                .paramDelete(req.params.author
                                            , req.params.title)
    itemCrud.deleteItems(deletePostParam, res, onSuccess = function(data){
        console.log('delete Post done!')
        res.send('Success')
        // write file

    })
});

// Helper functions
// Post - get param
getPostParam = function(req){
    return posts.PostItemParam()
            .paramGet().getItem(req.params.author, req.params.title)
} 

function getS3Img(){
    const s3 = new mod.AWS.S3()
    const myBucket = 'magazinewebimg'
    let params = {Bucket: myBucket, Key: '2015-fashion-trends.jpg'};
    s3.getObject(params, function(err, data) {
        if (err){
            console.log(err, err.stack);
        } else{
            console.log(data.Body);
            // return data.Body
        }
    });
}
// function reduce call back by 2 here 

// check if admin login
function checkAdmin(req, res, next){
    if(mod.isLogin){
      next();
    }else{
      res.redirect('/admin/login');
    }
}

module.exports = router;
