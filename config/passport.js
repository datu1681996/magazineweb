const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/Users');
// const config = require('../config/database');
const bcrypt = require('bcryptjs');
const itemCrud = require('../DataAccess/dynamoDB/ItemsCRUD')

module.exports = function(passport){
  // Local Strategy
  passport.use(new LocalStrategy(
                    // { passReqToCallback : true }, 
                    function(username, password, done) {
    // Match Username
    let query = {username:username};
    // User.findOne(query, function(err, user){
     
    // });
    console.log('user name =', username)
    const userParam = User.UserItemParam()
                          .paramGet()
                          .searchByUserId(username)
    const userGetParam = User.UserItemParam()
                          .paramGet()
                          .getItem('Admin', username)
    // itemCrud.searchItems(userParam, null, onSuccess = function(user){
     
    // })

    itemCrud.get_Items(userGetParam, null, onSuccess = function(user){
      // if(err) throw err;
      // if(!user){
      //   return done(null, false, {message: 'No user found'});
      // }
     
      if(user == undefined || user.Item == undefined){
        return done(null, false, {message: 'No user found'});
      }

      console.log("get pass =",  user['Item']['info']['password'])
      // Match Password
      bcrypt.compare(password, user['Item']['info']['password'], function(err, isMatch){
        if(err) throw err;
        // if(isMatch){
        //   return done(null, user);
        // } else {
        //   console.log("fine one")
        //   return done(null, false, {message: 'Wrong password'});
        // }

        console.log("get password =", password)
        console.log("get isMatch =", isMatch)
       
      });

      if(password == user['Item']['info']['password']){
        return done(null, user);
      } else {
        console.log("not found")
        return done(null, false, {message: 'Wrong password'});
      }

     
    });

  }));

  passport.serializeUser(function(user, done) {
    // done(null, user.id);
    done(null, user.userId);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });
}
