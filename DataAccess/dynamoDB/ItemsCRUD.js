const mod = require('../../Common/Mod')

mod.config()

const docClient = new mod.AWS.DynamoDB.DocumentClient();

module.exports = {
    createItems: function (Param, result, onSuccess = (data)=>{}) {
        const param = Param
        docClient.put(param, function (error, data) {
            if (error) {
                console.log("unable to create Item. Error JSON: ", JSON.stringify(error, null, 2))
                mod.errorMess(result, "Create Item")
            } else {
                console.log(" Create item success. JSON data: ", JSON.stringify(data, null, 2))
                mod.succMess(result, "Create Item")
                onSuccess(data)
            }
        })
    },
    deleteItems: function (Param, result, onSuccess = (data)=>{}) {
        const param = Param
        docClient.delete(param, function (err, data) {
            if (err) {
                console.log("unable to delete Item. Error JSON: ", JSON.stringify(err, null, 2))
                mod.errorMess(result, "Delete Item")
            } else {
                console.log(" Delete item success. JSON data: ", JSON.stringify(data, null, 2))
                mod.succMess(result, "Delete Item")
                onSuccess(data)
            }
        })
    },
    updateItems: function (Param, result, onSuccess = (data)=>{}) {
        const param = Param

        docClient.update(param, function (err, data) {
            if (err) {
                console.log("unable to update Item. Error JSON: ", JSON.stringify(err, null, 2))
                mod.errorMess(result, "update Item")
            } else {
                console.log(" update item success. JSON data: ", JSON.stringify(data, null, 2))
                mod.succMess(result, "update Item")
                onSuccess(data)
            }
        })
    },
    searchItems: function (Param, result,  onSuccess = (data)=>{}) {
        const param = Param
        docClient.query(param, function (err, data) {
            if (err) {
                console.log("unable to search Item. Error JSON: ", JSON.stringify(err, null, 2))
                mod.errorMess(result, "search Item")
            } else {
                console.log(" search item success. JSON data: ", JSON.stringify(data, null, 2))
                mod.succMess(result, "search Item", function () {
                    data.Items.forEach(function (item) {
                        console.log("Item:", item)
                    })
                })
                onSuccess(data)
            }
        })
    },
    get_Items: function (Param, result, onSuccess = (data)=>{}) {
        const params = Param

        docClient.get(params, function (err, data) {
            if (err) {
                console.log("unable to get Item. Error JSON: ", JSON.stringify(err, null, 2))
                mod.errorMess(result, "get Item")
            } else {
                console.log(" get item success. JSON data: ", JSON.stringify(data, null, 2))
                mod.succMess(result, "get Item")
                onSuccess(data)
            }
        })
    }
}