const mod = require('../../Common/Mod')

mod.config()

const dynamoDB = new mod.AWS.DynamoDB();
const docClient = new mod.AWS.DynamoDB.DocumentClient();

module.exports = {
    createTables: function (Param, result, onSuccess = ()=>{}) {
        const param = Param

        console.log("Create table : ")
        dynamoDB.createTable(param, function (err, data) {
            if (err) {
                console.log("Unable to create table. Error JSON: ", JSON.stringify(err, null, 2))
                mod.errorMess(result, "CREATE Table")
            } else {
                console.log("Create table success. JSON data: ", JSON.stringify(data, null, 2))
                mod.succMess(result, "CREATE Table")
               
            }
            onSuccess()
        })
    },
    deleteTables: function (Param, result) {
        const param = Param

        console.log("Detele table : ")
        dynamoDB.deleteTable(param, function (err, data) {
            if (err) {
                console.log("Unable to delete table. Error JSON: ", JSON.stringify(err, null, 2))
                mod.errorMess(result, "DELETE Table")
            } else {
                console.log("Delete table success. JSON data: ", JSON.stringify(data, null, 2))
                mod.succMess(result, "DELETE Table")
            }
        })
    },
    loadTables: function (Param, result, data, showUpdate = (res, item)=>{}) {
        console.log("Importing, please wait...")
        // const movieData = "../dataSource/moviedata.json"
        const allItems = JSON.parse(mod.fs.readFileSync(data, 'utf8'))
        // result.writeHead(200, {'Content-Type': 'text/html'})

        allItems.forEach(function (item) {
            // param update here
            const  param = Param.param(item)

            docClient.put(param, function (error, data) {
                if (error) {
                    console.log("unable to Add data. Error JSON: ", JSON.stringify(error, null, 2))
                } else {
                    // console.log(" Put item success ", item.title)
                    // result.write("<br> Item:" + item.title + " - Year: " + item.year)

                    // Param.showUpdate(result) // --> use this in callback instead
                    showUpdate(result, item)
                }
            })
        })

    },
    scanTables: function (tableName, onSuccess  = (data)=>{}) {
        let scanParam = {TableName: tableName}
        docClient.scan(scanParam, function (err, data) {
            if (err) {
                console.log("ERROR scan data")
            } else {
                console.log("item scanned = ", JSON.stringify(data, null, 2))
                onSuccess ( data )
            }
        })
    },
    scanTables_Param: function (scanParam, onSuccess  = (data)=>{}) {
        docClient.scan(scanParam, function (err, data) {
            if (err) {
                console.log("ERROR scan data")
            } else {
                console.log("item scanned = ", JSON.stringify(data, null, 2))
                onSuccess ( data )
            }
        })
    }
}
