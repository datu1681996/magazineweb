module.exports = {
    UserTableParam: function (tableName = "Users") {
        this.paramCreate = function () {
            return {
                TableName: tableName,
                KeySchema: [
                    {AttributeName: "role", KeyType: "HASH"},
                    {AttributeName: "userId", KeyType: "RANGE"}
                ],
                AttributeDefinitions: [
                    {AttributeName: "role", AttributeType: "S"},
                    {AttributeName: "userId", AttributeType: "S"},
                ],
                ProvisionedThroughput: {
                    WriteCapacityUnits: 10,
                    ReadCapacityUnits: 10
                }
            }
        }

        this.paramDelete = function () {
            return { TableName: tableName }
        }

        // update table by import data into table
        this.paramUpdate = function () {
            this.param = function (item) { // this work ??
                const data = {
                    TableName: tableName,
                    Item: {
                        "role": item.role,
                        "userId": item.userId,
                        "info": item.info
                    }
                }

                console.log("user update json =",  data)
                return data
            }

            this.showUpdate = function (item) { // put this here???
                console.log(" Put item success ", item.userId)
            }

            return this
        }

        // scan param
        this.searchByUserName = function (userName) {
            return {
                TableName: tableName,
                FilterExpression: "contains(info.userName, :uName)",
                ExpressionAttributeValues: {
                    ":uName": userName
                }
            }
        }
        
        return this
    },
    UserItemParam: function (tableName = "Users") {
        this.paramCreate = function (role, userId, info) {
            return {
                TableName: tableName,
                Item: {
                    "role": role,
                    "userId": userId,
                    "info": info
                }
            }
        }

        this.paramUpdate = function (role, userId, info) {
            return {
                TableName: tableName,
                Key: {
                    "role": role,
                    "userId": userId
                },
                UpdateExpression: "set info = :i",
                ExpressionAttributeValues: {
                    ":i": info
                },
                ReturnValues: "UPDATED_NEW"
            }
        }

        this.paramDelete = function (role, userId) {
            return {
                TableName: tableName,
                Key: {
                    "role": role,
                    "userId": userId
                }
            }
        }

        this.paramGet = function () {
            this.searchByUserId = function (userId) {
                return {
                    TableName: tableName,
                    KeyConditionExpression: "#u = :uid",
                    ExpressionAttributeNames: {
                        "#u": "userId"
                    },
                    ExpressionAttributeValues: {
                        ":uid": userId
                    }
                }
            }

            this.searchByRole = function (role) {
                return {
                    TableName: tableName,
                    KeyConditionExpression: "#r = :rol",
                    ExpressionAttributeNames: {
                        "#r": "role"
                    },
                    ExpressionAttributeValues: {
                        ":rol": role
                    }
                }
            }

            this.searchByUserName = function (userName) {
                return {
                    TableName: tableName,
                    KeyConditionExpression: "info.userName = :uName",
                    ExpressionAttributeValues: {
                        ":uName": userName
                    }
                }
            }

            this.getItem = function (role, userId) {
                return {
                    TableName: tableName,
                    Key: {
                        "role": role,
                        "userId": userId
                    }
                }
            }

            return this
        }

        return this
    }
}
