module.exports = {
    PostTableParam: function (tableName = "Posts") {
        this.paramCreate = function () {
            return {
                TableName: tableName,
                KeySchema: [
                    {AttributeName: "author", KeyType: "HASH"},
                    {AttributeName: "title", KeyType: "RANGE"}
                ],
                AttributeDefinitions: [
                    {AttributeName: "author", AttributeType: "S"},
                    {AttributeName: "title", AttributeType: "S"},
                ],
                ProvisionedThroughput: {
                    WriteCapacityUnits: 10,
                    ReadCapacityUnits: 10
                }
            }
        }

        this.paramDelete = function () {
            return { TableName: tableName }
        }

        // update table by import data into table
        this.paramUpdate = function () {
            this.param = function (item) { // this work ??
                const data = {
                    TableName: tableName,
                    Item: {
                        "author": item.author,
                        "title": item.title,
                        "content": item.content
                    }
                }

                console.log("post update json =",  data)
                return data
            }

            this.showUpdate = function (item) { // put this here???
                console.log(" Put item success ", item.title)
            }

            return this
        }
        
        // scan param
        this.searchByCate = function (category) {
            return {
                TableName: tableName,
                FilterExpression: "contains(content.category, :cate)",
                // ExpressionAttributeNames: {
                //     "#c": "content.category"
                // },
                ExpressionAttributeValues: {
                    ":cate": category
                }  
            }
        }

        return this
    },
    PostItemParam: function (tableName = "Posts") {
        this.paramCreate = function (author, title, content) {
            return {
                TableName: tableName,
                Item: {
                    "author": author,
                    "title": title,
                    "content": content
                }
            }
        }

        this.paramUpdate = function (author, title, content) {
            return {
                TableName: tableName,
                Key: {
                    "author": author,
                    "title": title
                },
                UpdateExpression: "set content = :con"
                                ,
                ExpressionAttributeValues: {
                    ":con": content
                },
                ReturnValues: "UPDATED_NEW"
            }
        }

        this.paramDelete = function (author, title) {
            return {
                TableName: tableName,
                Key: {
                    "author": author,
                    "title": title
                }
            }
        }

        this.paramGet = function () {
            this.searchItemByAuthor = function (author) {
                return {
                    TableName: tableName,
                    KeyConditionExpression: "#a = :auth",
                    ExpressionAttributeNames: {
                        "#a": "author"
                    },
                    ExpressionAttributeValues: {
                        ":auth": author
                    }
                }
            }

            this.searchItemByTitle = function (title) {
                return {
                    TableName: tableName,
                    KeyConditionExpression: "#t = :til",
                    ExpressionAttributeNames: {
                        "#t": "title"
                    },
                    ExpressionAttributeValues: {
                        ":til": title
                    }
                }
            }

            this.getItem = function (author, title) {
                return {
                    TableName: tableName,
                    Key: {
                        "author": author,
                        "title": title
                    }
                }
            }

            return this
        }

        this.paramGetItem = function (author, title) {
            return {
                TableName: tableName,
                Key: {
                    "author": author,
                    "title": title
                }
            }
        }

        return this
    }
}
