// JUST for test -> REMOVE later
module.exports = {
    MovieTableParam: function (tableName = "Movies") {
        this.paramCreate = function () {
            return {
                TableName: tableName,
                KeySchema: [
                    {AttributeName: "year", KeyType: "HASH"},
                    {AttributeName: "title", KeyType: "RANGE"}
                ],
                AttributeDefinitions: [
                    {AttributeName: "year", AttributeType: "N"},
                    {AttributeName: "title", AttributeType: "S"},
                ],
                ProvisionedThroughput: {
                    WriteCapacityUnits: 10,
                    ReadCapacityUnits: 10
                }
            }
        }

        this.paramDelete = function () {
            return { TableName: tableName }
        }

        // update table by import data into table
        this.paramUpdate = function (item) {
            this.param = function () { // this work ??
                const data = {
                    TableName: tableName,
                    Item: {
                        "year": item.year,
                        "title": item.title,
                        "info": item.info
                    }
                }

                console.log("movie update json =",  data)
                return data
            }

            this.showUpdate = function (result) { // put this here???
                console.log(" Put item success ", item.title)
                result.write("<br> Item:" + item.title + " - Year: " + item.year)
            }

            return this
        }

        return this
    },
    MovieItemParam: function (tableName = "Movies") {
        this.paramCreate = function (year, title, plot, info) {
            return {
                TableName: tableName,
                Item: {
                    "year": year,
                    "title": title,
                    "info": info // a json here
                }
            }
        }

        this.paramUpdate = function (year, title, info) {
            return {
                TableName: tableName,
                Key: {
                    "year": year,
                    "title": title
                },
                UpdateExpression: "set info.rating = :r, info.plot = :p, info.actors = :a",
                ExpressionAttributeValues: {
                    ":r": info.rating,
                    ":p": info.plot,
                    ":a": info.actors // array here
                },
                ReturnValues: "UPDATED_NEW"
            }
        }

        this.paramDelete = function (year, title) {
            return {
                TableName: tableName,
                Key: {
                    "year": year,
                    "title": title
                }
            }
        }

        this.paramGet = function () {
            this.searchItem = function (year) {
                return {
                    TableName: tableName,
                    KeyConditionExpression: "#yr = :yyyy",
                    ExpressionAttributeNames: {
                        "#yr": "year"
                    },
                    ExpressionAttributeValues: {
                        ":yyyy": year
                    }
                }
            }

            this.getItem = function (year, title) {
                return {
                    TableName: tableName,
                    Key: {
                        "year": year,
                        "title": title
                    }
                }
            }
        }

        return this
    }
}


// const item1 = {
//     "year": 112,
//             "title": "title 41",
//             "info": "INFO 1"
// }
// const  a  = MovieTableParam().paramUpdate(item1)
// a.param()
// MovieTableParam().paramUpdate(item1).showUpdate()