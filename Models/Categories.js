module.exports = {
    CategoryTableParam: function (tableName = "Categories") {
        this.paramCreate = function () {
            return {
                TableName: tableName,
                KeySchema: [
                    {AttributeName: "cateType", KeyType: "HASH"},
                    {AttributeName: "title", KeyType: "RANGE"}
                ],
                AttributeDefinitions: [
                    {AttributeName: "cateType", AttributeType: "S"},
                    {AttributeName: "title", AttributeType: "S"},
                ],
                ProvisionedThroughput: {
                    WriteCapacityUnits: 10,
                    ReadCapacityUnits: 10
                }
            }
        }

        this.paramDelete = function () {
            return { TableName: tableName }
        }

        // update table by import data into table
        this.paramUpdate = function () {
            this.param = function (item) { // this work ??
                const data = {
                    TableName: tableName,
                    Item: {
                        "cateType": item.cateType,
                        "title": item.title,
                        "info": item.info
                    }
                }

                console.log("category update json =",  data)
                return data
            }

            this.showUpdate = function (item) { // put this here???
                console.log(" Put item success ", item.title)
            }

            return this
        }

        return this
    },
    CategoryItemParam: function (tableName = "Categories") {
        this.paramCreate = function (cateType, title, info) {
            return {
                TableName: tableName,
                Item: {
                    "cateType": cateType,
                    "title": title,
                    "info": info
                }
            }
        }

        this.paramUpdate = function (cateType, title, info) {
            return {
                TableName: tableName,
                Key: {
                    "cateType": cateType,
                    "title": title
                },
                UpdateExpression: "set info = :i",
                ExpressionAttributeValues: {
                    ":i": info
                },
                ReturnValues: "UPDATED_NEW"
            }
        }

        this.paramDelete = function (cateType, title) {
            return {
                TableName: tableName,
                Key: {
                    "cateType": cateType,
                    "title": title
                }
            }
        }

        this.paramGet = function () {
            this.searchItemByTitle = function (title) {
                return {
                    TableName: tableName,
                    KeyConditionExpression: "#t = :til",
                    ExpressionAttributeNames: {
                        "#t": "title"
                    },
                    ExpressionAttributeValues: {
                        ":til": title
                    }
                }
            }

            this.searchItemByCateType = function (cateType) {
                return {
                    TableName: tableName,
                    KeyConditionExpression: "#c = :cate",
                    ExpressionAttributeNames: {
                        "#c": "cateType"
                    },
                    ExpressionAttributeValues: {
                        ":cate": cateType
                    }
                }
            }

            this.getItem = function (cateType, title) {
                return {
                    TableName: tableName,
                    Key: {
                        "cateType": cateType,
                        "title": title
                    }
                }
            }

            return this
        }

        return this
    }
}
