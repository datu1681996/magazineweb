var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// Tu Define
// use libraries
const flash = require('connect-flash');
const passport = require('passport')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views')); // re-define here???
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//// TU define
app.use(express.static(path.join(__dirname, 'PresentationLayer')));

//// TU define Routes
const indexRouter2 = require('./Controllers/IndexController');
const postRouter = require('./Controllers/PostController');
const adminHome = require('./Controllers/Admin/ManagerController');
const adminPostRouter = require('./Controllers/Admin/AdPostController');
const adminCateRouter = require('./Controllers/Admin/AdCategoriesController');
const adminUserRouter = require('./Controllers/Admin/AdUsersController');

// user pages
app.use('/', indexRouter2);
app.use('/users', usersRouter); // old route -> delete
app.use('/post.html', postRouter);

// admin pages
app.use('/admin', adminHome);
app.use('/admin/posts', adminPostRouter);
app.use('/admin/categories', adminCateRouter);
app.use('/admin/users', adminUserRouter);

// export libary to use in other files
// Express Messages Middleware
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

// // Passport Config
// require('./config/passport')(passport);
// // Passport Middleware
// app.use(passport.initialize());
// app.use(passport.session());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
