module.exports = {
    http: require('http'),
    url: require('url'),
    fs: require('fs'),
    AWS: require('aws-sdk'),
    cheerio: require('cheerio'),
    isLogin: false,
    // html root
    viewHtml: function(){
        const htmlDir = "./PresentationLayer/views/"
        const htmlAdminDir = "../PresentationLayer/views/Admin/"

        return Object.freeze({
            "catePage": htmlDir + "category.html"
            , "postPage": htmlDir + "post.html"
            , "contactPage": htmlDir + "contact.html"
            // roots for admin
            // Manager
            , "AdManager": htmlAdminDir + "Manager/manager" // .pug file
            , "AdLogin": htmlAdminDir + "Manager/login"
            // Category
            , "AdIndexCate": htmlAdminDir + "Category/indexCate"
            , "AdDetailCate": htmlAdminDir + "Category/detailCate"
            , "AdAddCate": htmlAdminDir + "Category/addCate"
            , "AdEditCate": htmlAdminDir + "Category/editCate"
            , "AdDeleteCate": htmlAdminDir + "Category/deleteCate"
            // Post
            , "AdIndexPost": htmlAdminDir + "Post/indexPost"
            , "AdDetailPost": htmlAdminDir + "Post/detailPost"
            , "AdAddPost": htmlAdminDir + "Post/addPost"
            , "AdEditPost": htmlAdminDir + "Post/editPost"
            , "AdDeletePost": htmlAdminDir + "Post/deletePost"
             // User
             , "AdIndexUser": htmlAdminDir + "User/indexUser"
             , "AdDetailUser": htmlAdminDir + "User/detailUser"
             , "AdAddUser": htmlAdminDir + "User/addUser"
             , "AdEditUser": htmlAdminDir + "User/editUser"
             , "AdDeleteUser": htmlAdminDir + "User/deleteUser"
        })
    },
    config: function () {
        this.AWS.config.update({
            accessKeyId: "AKIAJVLCXLK2BAB2XV2A",
            secretAccessKey: "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",
            region: 'us-west-2'
            // ,
            // endpoint: "http://localhost:8000"
        })
    },
    getURLQuery: function(urlLink) {
        console.log("url =", urlLink)
        const q = this.url.parse(urlLink)
        const qsub = this.url.parse(urlLink, true).query
        const tablename = qsub.tablename

        return { query: q, subQuery: qsub, tableName: tablename }
    },
    callBackRes: function (err, data, res) {
        if (err) {
            res.writeHead(404, {'Content-Type': 'text/html'})
            return res.end("404 not found")
        }
        res.writeHead(200, {'Content-Type': 'text/html'})
        res.write(data)
        return res.end()
    },
    callBackHTMLRes: function (err, data, res, handlePage = (page)=>{}, endMess = "") {
        let page = this.cheerio.load(data)

        if (err) {
            console.log("err = ", err)
            return res.end("404 not found")
        }

        handlePage(page)
        res.write(page.html())
        return res.end(endMess)
    },
    errorMess: function (res, strMess) {
        // res.writeHead(404, {'Content-type': 'text/html'})
        // res.write("FAILED TO " + strMess)
        console.log("FAILED TO " + strMess)
        // res.end()
    },
    succMess: function (res, strMess, callback = ()=>{}) {
        // res.writeHead(200, {'Content-type': 'text/html'})
        // res.write(strMess + " SUCCESS")
        console.log(strMess + " SUCCESS")
        callback()
        // res.end()
    },
    getCurrentDate: function() {
        const dateObj = new Date();
        const month = dateObj.getUTCMonth() + 1 //months from 1-12
        const day = dateObj.getUTCDate()
        const year = dateObj.getUTCFullYear()
        const hour = dateObj.getHours()
        const min = dateObj.getMinutes()

        return year + " - " + month + " - " + day 
                    + "   " + hour + ":" + min;
    }
}