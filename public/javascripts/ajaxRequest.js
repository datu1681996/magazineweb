// delete post
$(document).ready(function(){
    $('.delete-item').on('click', function(e){
      $target = $(e.target);
      const itemUrl = $target.attr('data-id');
      const itemCode = $target.attr('data-code');

      console.log("data id =", itemUrl)
      $.ajax({
        type:'DELETE',
        url: itemUrl,
        success: function(response){
          alert('Deleting item');
          window.location.href='/admin/' + itemCode;
          // window.location.href = response.redirect;
        },
        error: function(err){
          console.log(err);
          alert('Server error');
          window.location.href='/admin/posts';
        }
      });
    });
});

  // update post
$(document).ready(function(){
  $('.update-post').on('click', function(e){
      $target = $(e.target);
      const id = $target.attr('data-id');
      console.log("data id =", id)
      // $.ajax({
      //   type:'PUT',
      //   url: '/admin/posts/edit/'+id,
      //   success: function(response){
      //     alert('Putting Post');
      //     window.location.href='/admin/posts';
      //     // window.location.href = response.redirect;
      //   },
      //   error: function(err){
      //     console.log(err);
      //     alert('Server error');
      //     window.location.href='/admin/posts';
      //   }
      // });
  });
});
  